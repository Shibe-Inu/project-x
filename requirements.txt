django>=3.1.7
python-docx>=0.8.10
docxcompose>=1.3.1
pdf2docx>=0.5.1
python-magic>=0.4.22
python-magic-bin>=0.4.14
