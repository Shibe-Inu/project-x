from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
import matplotlib.pyplot as plt
from django.urls import reverse
from .forms import GraphData
import io

names = []
values = []

def return_graph(names, values):
    plt.ylabel('some numbers')

    plt.bar(names, values)

    imgdata = io.StringIO()
    plt.savefig(imgdata, format='svg')
    imgdata.seek(0)

    data = imgdata.getvalue()
    return data

def get_graph(request):
    if request.method == 'POST':
        form = GraphData(request.POST)
        if form.is_valid():
            for item in form.fields:
                if isinstance(form.cleaned_data[item], float):
                    values.append((form.cleaned_data[item]))
                else:
                    names.append((form.cleaned_data[item]))
    else:
        form = GraphData()

    return form



def graphs(request):
    ctx = {}
    ctx["graph"] = return_graph(names, values)
    ctx["form"] = get_graph(request)
    return render(request, "graphs.html", ctx)
