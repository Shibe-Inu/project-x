from django import forms

class GraphData(forms.Form):
    name1 = forms.CharField(max_length=100)
    value1 = forms.FloatField()
    name2 = forms.CharField(max_length=100)
    value2 = forms.FloatField()
    name3 = forms.CharField(max_length=100)
    value3 = forms.FloatField()
