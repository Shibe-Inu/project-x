# CRABDOC
Проект для быстрой и удобной работы с документами и информацией, нацеленный на учеников вузов и институтов.

## ГОТОВЫЙ ФУНКЦИОНАЛ
1.  [x] Извлечение текста из картинок
2.  [x] Построение графиков
3.  [ ] API
4.  [ ] Стилизация и мерджинг документов

# CRABDOC 
A website for fast and simple data manipulation made specifically for university students.

## TODO
1.  [x] Text extraction from images
2.  [x] Graph plotting
3.  [ ] API
4.  [ ] Document stylization and merging 
